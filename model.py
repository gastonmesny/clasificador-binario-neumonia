
def train_model(x_train, y_train, x_test, y_test):
    
    x_train, y_train = x_train, y_train
    x_train = x_train / 255.0
    x_test  = x_test  / 255.0 
      
    model = tf.keras.models.Sequential([
        tf.keras.layers.Flatten(),
        tf.keras.layers.Dense(64, activation=tf.nn.relu),
        tf.keras.layers.Dense( 1, activation=tf.nn.sigmoid)
    ])

    model.compile(optimizer='adam',
                  loss='binary_crossentropy',
                  metrics=['accuracy'])
    
    history = model.fit(x_train, y_train, epochs=10, validation_data=(x_test, y_test))
    
    return history